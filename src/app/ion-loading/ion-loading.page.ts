import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-ion-loading',
  templateUrl: './ion-loading.page.html',
  styleUrls: ['./ion-loading.page.scss'],
})
export class IonLoadingPage {

  constructor(
    public loadingCtrl: LoadingController,
  ) { }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({     
      //duration: 5000
    });
    await loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 3000);

  }

  async presentLoadingOptions() {
    const loading = await this.loadingCtrl.create({
      duration: 5000,
      message: 'Mostrando Loading Opción 2',
    });
    await loading.present();

  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingCtrl.create({
      spinner: null,  //dots, hide
      duration: 5000,
     // animated: true,
      message: 'Mostrando Loading Opción 3',
      translucent: true,
      cssClass: 'transparent',
      backdropDismiss: true
    });
    await loading.present();

  }
}